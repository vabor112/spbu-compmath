# -*- coding: utf-8 -*-

import sys, os, glob
from numpy import poly1d, polyval, arange, exp
from math import factorial, sqrt, cos, pi, sin
import matplotlib.pyplot as plt
import csv

from decimal import Decimal, getcontext
from math import log10

############################### General stuff #################################

def matrix_print(matrix):
    for row in matrix:
        for element in row:
            print element,
        print ''

def poly1d_to_tex(p, variable = 'x'):
    result = ''
    for i in xrange(p.o):
        if p.c[i] != 0:
            if p.o - i != 1:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s^%s'
                elif p.c[i] > 0:
                    template = '+ %s %s^%s'
                result += template % (p.c[i], variable, p.o - i)
            else:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s'
                elif p.c[i] > 0:
                    template = '+ %s %s'
                result += template % (p.c[i], variable)
    if (p.c[p.o] != 0) or (result == ''):
        if (p.c[p.o] <= 0) or (result == ''):
            template = '%s'
        elif p.c[p.o] > 0:
            template = '+ %s'
        result += template % p.c[p.o]
    return result

def sign(x):
    if x > 0:
        return 1
    elif x < 0:
        return -1
    elif x == 0:
        return 0

def diff_sign(diff_order):
    if diff_order <= 3:
        return '\'' * diff_order
    else:
        return '^{diff_order}'
    o

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

###############################################################################

def numerical_differentiation(f, diff_order, nth_diff_of_f, num_diff_formula, x_0, h, n,
		f_tex = '', n_diff_tex = '', DERIVS_TABLE_NAME = 'test_table_numdiff.csv', OUTPUT_NAME = 'derivs'):
    test_table_header = ['$x$', '$f(x)$', '$f%s(x)$' % diff_sign(diff_order),'$f%s(x)$ (числ.)' % diff_sign(diff_order), 'погрешность']
    # Some shitcode here, fuck it.
    test_table_values = [[x_0 + i*h, f(x_0 + i*h), nth_diff_of_f(x_0 + i*h), num_diff_formula(f, x_0 + i*h, h), abs(nth_diff_of_f(x_0 + i*h) - num_diff_formula(f, x_0 + i*h, h))] for i in xrange(0, n + 1)]
    test_table = [test_table_header] + test_table_values

    with open(DERIVS_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows(test_table)

    from jinja2 import Environment, FileSystemLoader
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('derivs_template.tex')
    output_from_parsed_template = template.render(f = f_tex, n_diff_tex = n_diff_tex,
			DERIVS_TABLE_NAME = DERIVS_TABLE_NAME)

    with open(OUTPUT_NAME + '.tex', 'w') as f:
        f.write(unicode(output_from_parsed_template).encode('utf-8'))

    import subprocess, shlex
    proc=subprocess.Popen(shlex.split('xelatex %s.tex' % OUTPUT_NAME))
    proc.communicate()
    map(os.unlink, glob.glob(u'*.log'))
    map(os.unlink, glob.glob(u'*.aux'))

    print 'Pdf file is successfully generated, look for hermite.pdf in current folder.'

def numdiff_optimal_h_test(f, x, diff_order, nth_diff_of_f, num_diff_formula, h_optimal,
        OPTIMAL_H_TABLE_NAME = 'optimal_h_numdiff.csv'):
    from decimal import Decimal, getcontext
    from math import log10
    getcontext().prec = 6
    print log10(h_optimal)
    print getcontext().prec
    h = Decimal(0.1)
    NUM_STEPS = 7
    table_header = ['$h$'] + [h**(i) for i in xrange(NUM_STEPS)]
    table_header_verb = ['$h$'] + [('степень %s' % i) for i in xrange(NUM_STEPS)]
    table_row1 = ['$f%s(x)$' % diff_sign(diff_order)] + [num_diff_formula(f, x, table_header[1 + i]) for i in xrange(NUM_STEPS)]
    table_row2 = ['погрешность'] + [abs(nth_diff_of_f(x) - table_row1[i + 1]) for i in xrange(NUM_STEPS)]

    with open(OPTIMAL_H_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows([table_header, table_header_verb, table_row1, table_row2])

def numdiff_optimal_h_test(f, x, diff_order, nth_diff_of_f, num_diff_formula, precision, list_of_h_to_try,
        OPTIMAL_H_TABLE_NAME = 'optimal_h_numdiff.csv'):
    getcontext().prec = precision
    NUM_STEPS = len(list_of_h_to_try)
    table_header = ['$h$'] + list_of_h_to_try
    table_header_verb = ['$h$'] + [('степень %s' % i) for i in xrange(NUM_STEPS)]
    table_row1 = ['$f%s(x)$' % diff_sign(diff_order)] + [num_diff_formula(f, x, table_header[1 + i]) for i in xrange(NUM_STEPS)]
    table_row2 = ['погрешность'] + [abs(nth_diff_of_f(x) - table_row1[i + 1]) for i in xrange(NUM_STEPS)]

    with open(OPTIMAL_H_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows([table_header, table_header_verb, table_row1, table_row2])

def lagrange_interpolation(nodes, values, power):
    approximations = []
    for p in xrange(power + 1):
        approximation = poly1d([0])
        for i in xrange(p + 1):
            cur_approximation = poly1d([1])
            for j in xrange(p + 1):
                if i != j:
                    cur_approximation = cur_approximation * poly1d([nodes[j]], True)
                    cur_approximation = cur_approximation / (nodes[i] - nodes[j])
            #print cur_approximation
            cur_approximation = cur_approximation * values[i]
            #print cur_approximation
            approximation = approximation + cur_approximation
        approximations.append(approximation)
    return approximations

def diff_formula(nodes, values, power, target_point):
	nodes = sorted(nodes, key = lambda x: abs(x - target_point))


f = lambda x : x**2 + 2


# f' O(h)
numerical_differentiation(f, 1, lambda x: 2*x, lambda f, x, h: (f(x + h) - f(x))/(h), 0, 0.1, 10, f_tex = 'x^2+2', n_diff_tex = '\\frac{f(x+h)-f(x)}{h}', OUTPUT_NAME = 'derivs_1_1')
# f' O(h^2)

numerical_differentiation(f, 1, lambda x: 2*x, lambda f, x, h: (f(x + h) - f(x - h))/(2*h), 0, 0.1, 10, f_tex = 'x^2+2', n_diff_tex = '\\frac{f(x+h)-f(x-h)}{2h}', OUTPUT_NAME = 'derivs1_2')

# f'' O(h^2)
numerical_differentiation(f, 2, lambda x: 2, lambda f, x, h: (f(x + h) - 2*f(x) + f(x-h))/(h**2), 0, 0.1, 10, f_tex = 'x^2+2', n_diff_tex = '\\frac{f(x+h)-2f(x)+f(x-h)}{h^2}', OUTPUT_NAME = 'derivs2_1')

#numdiff_optimal_h_test(lambda x: x**4 + 2, 1, 1, lambda x: 4*x**3, lambda f, x, h: (f(x + h) - f(x - h))/(2*h), 10**(-10))

numdiff_optimal_h_test(lambda x: sin(x), 1, 1, lambda x: cos(x), lambda f, x, h: (f(x + h) - f(x - h))/float(2*h), 9, [Decimal(2)**(-i) for i in xrange(6,15)])

