/* Simple matrix class. */

#pragma once

#include <istream>
#include <ostream>
#include <vector>


namespace vabor112 {

	template <class T> class Matrix;

	template <class T> Matrix<T> operator+(const Matrix<T>& left,
			const Matrix<T>& right);
	template <class T> Matrix<T> operator-(const Matrix<T>& left,
			const Matrix<T>& right);
	template <class T> Matrix<T> operator-(const Matrix<T>& matrix);
	template <class T> Matrix<T> operator*(const Matrix<T>& left,
			const Matrix<T>& right);
	template <class T> Matrix<T> operator*(const Matrix<T>& matrix,
			const T& scalar);
	template <class T> Matrix<T> operator*(const T& scalar,
			const Matrix<T>& matrix);
	template <class T> std::vector<T> operator* (const Matrix<T>& matrix,
			const std::vector<T>& vector);

	template <class T> class Matrix {
		public:
			Matrix();

			Matrix(size_t width, size_t height);

			//Matrix(const Matrix<T>& from);

			// Don't use this fuckup
			template <class A> Matrix(const Matrix<A>& from) {
				width = from.get_width(); height = from.get_height();
				rep = new T[width * height];
				for (size_t i = 0; i < width; i++) {
					for (size_t j = 0; j < height; j++) {
						(*this)(i, j) = static_cast<T>(from(i, j));
					}
				}
			}

			inline size_t get_width() const { return width; }
			inline size_t get_height() const { return height; }
		
		private:
			size_t width, height;

			T* rep; // rep stands for "representation".

		public:
			/////////////////////////// Exceptions ////////////////////////////

			class MathException {};

			class WrongDimensionException : public MathException {
				public:
					WrongDimensionException() { }
			};
			
			class DegenerateMatrixException : public MathException {};

			class IOException {};

			class FormatViolationException : public IOException {};

			///////////////////////// Element access //////////////////////////

			// To get `x` in matrix
			//
			//	   * * x *
			// m = * * * *
			//	   * * * *
			//
			// you call `m(2,0)`.
			T& operator()(size_t row, size_t column);
			const T& operator()(size_t row, size_t column) const;

			//////////////////////////// Assignment ///////////////////////////

			Matrix<T>& operator=(const Matrix<T>& matrix);
			Matrix<T>& operator+=(const Matrix<T>& matrix);
			Matrix<T>& operator-=(const Matrix<T>& matrix);
			Matrix<T>& operator*=(const Matrix<T>& matrix);

			//////////////////////////// IO operators /////////////////////////

			friend std::ostream& operator<<(std::ostream& output,
					const Matrix<T>& matrix) {
				for(size_t i = 0; i < matrix.height; i++) {
					for(size_t j = 0; j < matrix.width; j++) {
						output << matrix(j, i);
						if (j+1 != matrix.width) output << ' ';
					}
					if (i+1 != matrix.height) output << '\n';
				}

				return output;
			}

			friend std::istream& operator>>(std::istream& input,
					Matrix<T>& matrix) {
				std::vector<T> rep_vec; T cur_item; size_t width = 1; size_t height = 1;
				while(true) {
					if (!(input >> cur_item)) {
						break;
					}

					rep_vec.push_back(cur_item);
					if ((input.peek() == ' ') && (height == 1)) width += 1;
					if (input.peek() == '\n') height += 1;

					// `input.get()` actually returns an int. The result
					//  is set to -1 if EOF character is encountered.
					int next = input.get();
					if (next == EOF) break;
					int nextnext = input.get();
					if((next == '\n') && (nextnext == '\n')) {
						break;
					} else {
						input.unget(); input.unget();
					}
				}

				height = height - 1; // Hack.
				
				if (rep_vec.size() != width*height) {
					// std::cout << "vector size: " << rep_vec.size() << std::endl;
					// std::cout << "width, height = " << width << ", " << height << std::endl;
					throw FormatViolationException();
				}
				if (rep_vec.size() != width*height) throw FormatViolationException();
				matrix.~Matrix(); // Initial cleanup.
				matrix.width = width; matrix.height = height; matrix.rep = new T[width*height];
				std::copy(rep_vec.begin(), rep_vec.end(), matrix.rep);
				return input;
			}

			//////////////////////////// Arithmetic ///////////////////////////

			friend Matrix<T> operator+ <> (const Matrix<T>& left,
					const Matrix<T>& right);

			friend Matrix<T> operator- <> (const Matrix<T>& left,
					const Matrix<T>& right);
			friend Matrix<T> operator- <> (const Matrix<T>& matrix);

			friend Matrix<T> operator* <> (const Matrix<T>& left,
					const Matrix<T>& right);
			friend Matrix<T> operator* <> (const Matrix<T>& matrix,
					const T& scalar);
			friend Matrix<T> operator* <> (const T& scalar,
					const Matrix<T>& matrix);

			friend std::vector<T> operator* <> (const Matrix<T>& matrix,
					const std::vector<T>& vector);

			Matrix<T> transpose() const {
				Matrix<T> result(height, width);
				for (size_t i = 0; i < height; i++) {
					for (size_t j = 0; j < width; j++) {
						result(i, j) = (*this)(j, i);
					}
				}
				return result;
			}

			// TODO: implement matrix inverse and following operators.
			// friend template <class T> Matrix<T> operator/(const Matrix<T>& matrix,
			// 		const T& scalar);
			// friend template <class T> Matrix<T> operator/(const T& scalar,
			// 		const Matrix<T>& matrix); // Attention: reverses matrix!
			// friend template <class T> Matrix<T> operator/(const Matrix<T>& left,
			// 		const Matrix<T>& right); // Attention: reverses matrix!

			//////////////////////////// Comparsion ///////////////////////////

			friend bool operator==(const Matrix<T>& left, const Matrix<T>& right);
			friend bool operator!=(const Matrix<T>& left, const Matrix<T>& right);

			/////////////////////////////// Stuff /////////////////////////////

			template <class A> operator Matrix<A>() {
				Matrix<A> result(width, height);
				for (size_t i = 0; i < width; i++) {
					for (size_t j = 0; j < height; j++) {
						result(i, j) = static_cast<A>((*this)(i, j));
					}
				}
				// complier won't let me use 
				// private members(`rep`) of `result` here, hence next two
				// lines won't compile.
				// for(size_t i = 0; i < width * height; i++)
				// 	result.rep[i] = static_cast<A>(rep[i]);
				return result;
			}

			void resize(size_t _width, size_t _height) {
				if (width*height == _width*_height) {
					width = _width;
					height = _height;
				} else {
					~Matrix();
					width = _width;
					height = _height;
					rep = new T[width*height];
				}
			}

			void fill(T value) {
				for (size_t i = 0; i < width*height; i++)
					rep[i] = value;
			}

			///////////////////////////// Static //////////////////////////////

			static Matrix<T> get_identity(size_t width, size_t height) {
				Matrix<T> result(width, height);
				for (size_t i = 0; i < width; i++) {
					for (size_t j = 0; j < height; j++) {
						// Attention: we cast int to T here!
						if (i != j) result(i, j) = 0; else result(i, j) = 1;
					}
				}
				return result;
			}
	};

}

#include "matrix.tpp"
