#include <fstream>
#include <iostream>
#include "matrix.h"


int main(int argc, char* argv[]) {
	std::ifstream input("input");
	std::ofstream output("output");


	vabor112::Matrix<double> D;
	vabor112::Matrix<double> C;
	vabor112::Matrix<double> B;

	vabor112::Matrix<double> matrix;
	vabor112::Matrix<double> LU;
	vabor112::Matrix<int> P;

	input >> D >> C >> B;

	matrix = D + 9.0*C;
// 	matrix = vabor112::Matrix<double>::get_identity(4,4);
// 	matrix(0,0) = 1.0;
// 	matrix(1,1) = 2.0;
// 	matrix(2,2) = 3.0;
// 	matrix(3,3) = 4.0;


	output << "Solving A*X=B for:\n\n";
	output << "A:\n" << matrix << "\n\n";
	output << "B:\n" << B << "\n";

	// std::cout << D << "\n\n" << C << "\n\n" << matrix << std::endl;

	// matrix -= matrix;
	// output << matrix;
	LUP_decomposition(matrix, LU, P);

	vabor112::Matrix<double> L(matrix.get_width(), matrix.get_height());
	vabor112::Matrix<double> U(matrix.get_width(), matrix.get_height());

	for (size_t i = 0; i < LU.get_height(); i++) {
		for (size_t j = 0; j < LU.get_width(); j++) {
			if (j < i) {
				L(j, i) = LU(j, i);
				U(j, i) = 0.0;
			} else if (i == j) {
				L(j, i) = 1.0;
				U(j, i) = LU(j, i);
			} else {
				L(j, i) = 0.0;
				U(j, i) = LU(j, i);
			}
		}
	}
	
	output << "\nLUP decomposition and its check:\n";
	
	output << "\nL:\n" << L;

	output << "\n\nU\n" << U;

	output << "\n\nP:\n" << P;

	output << "\n\nL*U:\n" << L*U;

	LU_decomposition(matrix, LU);

	for (size_t i = 0; i < LU.get_height(); i++) {
		for (size_t j = 0; j < LU.get_width(); j++) {
			if (j < i) {
				L(j, i) = LU(j, i);
				U(j, i) = 0.0;
			} else if (i == j) {
				L(j, i) = 1.0;
				U(j, i) = LU(j, i);
			} else {
				L(j, i) = 0.0;
				U(j, i) = LU(j, i);
			}
		}
	}

	output << "\n\nLU decomposition and its check:\n";
	
	output << "\nL:\n" << L;

	output << "\n\nU\n" << U;

	output << "\n\nL*U:\n" << L*U;

	output << "\n\nX(result):\n" << LUP_solve_for_matrix(LU, P, B);

	std::cout << "Success!" << std::endl;
}
