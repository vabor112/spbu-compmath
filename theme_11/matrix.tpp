#include <algorithm> // std::copy
#include <vector>
#include <cmath> // abs

////////////////////////////////// Constructors ///////////////////////////////

template <class T> vabor112::Matrix<T>::Matrix() : width(0), height(0) {}

template <class T> vabor112::Matrix<T>::Matrix(size_t _width, size_t _height)
	: width(_width), height(_height) {

	rep = new T[width*height];
}

//template <class T> vabor112::Matrix<T>::Matrix(const vabor112::Matrix<T>& matrix)
//	: width(matrix.width), height(matrix.height) {
//
//	// Lacks optimality here. Doesn't matter for now.
//	rep = new T[width*height];
//	std::copy(matrix.rep, matrix.rep + width*height, rep);
//}

///////////////////////////////// Access operators /////////////////////////////

template <class T> T& vabor112::Matrix<T>::operator()(size_t row, size_t column) {
	return rep[column*width + row];
}

template <class T> const T& vabor112::Matrix<T>::operator()(size_t row, size_t column) const {
	return rep[column*width + row];
}

//////////////////////////////// Assignment operators //////////////////////////

template <class T>
vabor112::Matrix<T>& vabor112::Matrix<T>::operator=(const vabor112::Matrix<T>& matrix) {
	if(this == &matrix)
		return *this;

	this -> ~Matrix(); // Initial cleanup;
	width = matrix.width; height = matrix.height; rep = new T[width*height];
	std::copy(matrix.rep, matrix.rep + width*height, rep);

	return *this;
}

template <class T>
vabor112::Matrix<T>& vabor112::Matrix<T>::operator+=(const vabor112::Matrix<T>& matrix) {
	if ((width != matrix.width) || (height != matrix.height))
		throw typename vabor112::Matrix<T>::WrongDimensionException();

	for(size_t i = 0; i < width*height; i++)
		rep[i] += matrix.rep[i];

	return *this;
}

template <class T>
vabor112::Matrix<T>& vabor112::Matrix<T>::operator-=(const vabor112::Matrix<T>& matrix) {
	if ((width != matrix.width) || (height != matrix.height))
		throw typename vabor112::Matrix<T>::WrongDimensionException();

	for(size_t i = 0; i < width*height; i++)
		rep[i] -= matrix.rep[i];

	return *this;
}

template <class T>
vabor112::Matrix<T>& vabor112::Matrix<T>::operator*=(const vabor112::Matrix<T>& matrix) {
	return (*this = *this * matrix); // There is no obvious way to multiply matrices "in place".
}

template <class T> vabor112::Matrix<T> vabor112::operator+(const vabor112::Matrix<T>& left,
		const vabor112::Matrix<T>& matrix) {
	if ((left.width != matrix.width) || (left.height != matrix.height)) {
		throw typename vabor112::Matrix<T>::WrongDimensionException();
	}
	vabor112::Matrix<T> result(left);
	for(size_t i = 0; i < left.width*left.height; i++)
		result.rep[i] += matrix.rep[i];
	return result;
}

//////////////////////////////// Arithmetic operators //////////////////////////

template <class T> vabor112::Matrix<T> vabor112::operator-(const vabor112::Matrix<T>& left,
		const vabor112::Matrix<T>& matrix) {
	if ((left.width != matrix.width) || (left.height != matrix.height))
		throw typename vabor112::Matrix<T>::WrongDimensionException();
	vabor112::Matrix<T> result(left);
	for(size_t i = 0; i < left.width*left.height; i++)
		result.rep[i] -= matrix.rep[i];
	return result;
}

template <class T> vabor112::Matrix<T> operator-(const vabor112::Matrix<T>& matrix) {
	vabor112::Matrix<T> result(matrix);
	for(size_t i = 0; i < matrix.width*matrix.height; i++)
		result.rep[i] = -result.rep[i];
	return result;
}

template <class T> vabor112::Matrix<T> vabor112::operator*(const vabor112::Matrix<T>& left,
		const vabor112::Matrix<T>& right) {
	if (left.width != right.height)
		throw typename vabor112::Matrix<T>::WrongDimensionException();

	size_t n = left.width; // equals to right.height

	vabor112::Matrix<T> result(right.width, left.height);
	for (size_t i = 0; i < result.width; i++) {
		for (size_t j = 0; j < result.height; j++) {
			result(i, j) = 0;
			//std::cout << "result("<< i<< "," << j << ") = " << result(i,j) << std::endl;
			for (size_t k = 0; k < n; k++) {
				result(i, j) += left(k, j)*right(i, k);
				//std::cout << "result("<< i<< "," << j << ") = " << result(i,j) << std::endl;
			}
		}
	}

	return result;
}

template <class T> vabor112::Matrix<T> vabor112::operator*(const T& scalar,
		const vabor112::Matrix<T>& matrix) {
	vabor112::Matrix<T> result(matrix);
	for(size_t i = 0; i < result.width*result.height; i++)
		result.rep[i] *= scalar;
	return result;
}

template <class T> vabor112::Matrix<T> vabor112::operator*(const vabor112::Matrix<T>& matrix,
		const T& scalar) {
	return scalar*matrix;
}

template <class T> std::vector<T> vabor112::operator*(
		const vabor112::Matrix<T>& matrix, const std::vector<T>& vector) {

	if (matrix.width != vector.size())
		throw typename vabor112::Matrix<T>::WrongDimensionException();

	std::vector<T> result(matrix.height);

	for (size_t i = 0; i < matrix.height; i++) {
		result[i] = 0;
		for(size_t j = 0; j < matrix.width; j++) {
			result[i] += matrix(j, i) * vector[j];
		}
	}

	return result;
}

//////////////////////////////// Comparison operators //////////////////////////

template <class T> bool operator==(const vabor112::Matrix<T>& left,
		const vabor112::Matrix<T>& right) {
	if ((left.width != right.width) || (left.height != right.height))
		return false;

	for (size_t i = 0; i < left.width * left.height; i++)
		if (left.rep[i] != right.rep[i])
			return false;

	return true;
}

template <class T> bool operator!=(const vabor112::Matrix<T>& left,
		const vabor112::Matrix<T>& right) {
	return !(left == right);
}

////////////////////////////////////// Stuff //////////////////////////////////

// void LUP_decomposition(const vabor112::Matrix<double>& matrix,
// 		vabor112::Matrix<double>& L, vabor112::Matrix<double>& U,
// 		vabor112::Matrix<double>& P) {

// `LU` matrix contains both L and U triangular matrices. Specifically, if
//
//      a b c           1 0 0          a b c
// LU = d e f  then L = d 1 0  and U = 0 e f  this is it.
//      g h i           g h 1          0 0 i
//
// If you provide LU and P matrices of appropriate sizes, then their
// storage(`rep`) will be reused, no additional overheads in such cases.
// I fucked it up ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^.

void LUP_decomposition(const vabor112::Matrix<double>& matrix,
		vabor112::Matrix<double>& LU, vabor112::Matrix<int>& P, bool pivoting = true) {

	if (matrix.get_width() != matrix.get_height()) // LUP only for square matrices.
		throw vabor112::Matrix<double>::WrongDimensionException();

	size_t n = matrix.get_width(); // equals to matrix.height

	LU = matrix; // MAY BE DANGEROUS.
	//P.resize(matrix.width, matrix.height);
	P = vabor112::Matrix<int>::get_identity(n, n);

	for (size_t k = 0; k < n; k++) {
		if (pivoting) {
			// Searching for element with maximal absolute value in `k` column
			// ("hack" to enhance computational stability).
			double cur_abs_max = 0.0;
			size_t cur_abs_max_position;
			for (size_t j = k; j < n; j++) {
				if (abs(LU(k, j)) > cur_abs_max) {
					cur_abs_max = LU(k, j);
					cur_abs_max_position = j;
				}
			}

			if (cur_abs_max == 0.0)
			// ^^^^^^^^^^^^^^^^^^^^ this means that every element in `k` column is 0.
				throw vabor112::Matrix<double>::DegenerateMatrixException();

			// Changing permutation matrix(swap `k` and `cur_abs_max_position` rows`)
			// and performing permutation to `LU` matrix(swap `k` and
			// `cur_abs_max_position` rows).
			for (size_t i = 0; i < n; i++) {
				std::swap(P(i, k), P(i, cur_abs_max_position));
				std::swap(LU(i, k), LU(i, cur_abs_max_position));
			}
		}

		// Actual step of algorithm.

		for (size_t j = k + 1; j < n; j++) {
			LU(k, j) = LU(k, j)/LU(k, k);
			for(size_t i = k + 1; i < n; i++) {
				LU(i, j) = LU(i, j) - LU(k, j)*LU(i, k);
			}
		}
	}
}

void LU_decomposition(const vabor112::Matrix<double>& matrix,
		vabor112::Matrix<double>& LU) {
	vabor112::Matrix<int> P;
	return LUP_decomposition(matrix, LU, P, false);
}

// std::vector<double> LUP_solve(const vabor112::Matrix<double>& LU,
// 		const vabor112::Matrix<int>& P, std::vector<double> b) {
// 	if (matrix.get_width() != matrix.get_height()) // LUP only for square matrices.
// 		throw vabor112::Matrix<double>::WrongDimensionException();
// 	if (matrix.get_width() != b.size())
// 		throw vabor112::Matrix<double>::WrongDimensionException();
// 
// 	b = P.transpose() * b;
// 
// 	// First let's solve L*x=`b`.
// 	std::vector<double> tmp(b.size());
// 	for (int i = 0; i < b.size(); i++) {
// 		tmp[i] = b[i];
// 		for (int j = 0; j < i; j++) {
// 			tmp[i] -= tmp[j]*LU(j, i);
// 		}
// 	}
// 
// 	// Second we need to solve U*x=`tmp`.
// 	std::vector<double> res(b.size());
// 	for (int i = b.size() - 1; i >= 0; i--) {
// 		result[i] = b[i];
// 		for (size_t j = b.size() - 1; j > i; j--) {
// 			result[i] -= result[j]*LU(j, i);
// 			result[i] = result[i] / LU(i,i);
// 		}
// 	}
// 
// 	return result;
// }

vabor112::Matrix<double> LUP_solve_for_matrix(const vabor112::Matrix<double>& LU,
		const vabor112::Matrix<int>& P, vabor112::Matrix<double> B) {
	if (LU.get_width() != LU.get_height()) // LUP only for square matrices.
		throw vabor112::Matrix<double>::WrongDimensionException();
	if (LU.get_width() != B.get_height())
		throw vabor112::Matrix<double>::WrongDimensionException();

	//std::cout << "LAYL\n\n" << P << "\n\nLAYL\n\n";
	//std::cout << "LAYL B \n\n" << B << "\n\nLAYL\n\n";
	//std::cout << "LAYL TRANSPPOSED\n\n" << P.transpose() << "\n\nLAYL\n\n";
	//std::cout << "LAYL CASTED\n\n" << static_cast<vabor112::Matrix<double>>(P) << "\n\nLAYL\n\n";
	//std::cout << "LAYL TRANSPPOSED CASTED\n\n" << static_cast<vabor112::Matrix<double>>(P.transpose()) << "\n\nLAYL\n\n";
	//std::cout << "LAYL TRANSPPOSED CASTED MULTIPLIED\n\n" << static_cast<vabor112::Matrix<double>>(P.transpose()) * B << "\n\nLAYL\n\n";
	//std::cout << "LAYL CASTED MULTIPLIED\n\n" << static_cast<vabor112::Matrix<double>>(P) * B << "\n\nLAYL\n\n";
	vabor112::Matrix<double> PD = static_cast<vabor112::Matrix<double>>(P);
	//std::cout << "PD:\n\n" << PD << "\n\nLAYL\n\n";
	B = PD * B;
	//B = static_cast<vabor112::Matrix<double>>(P.transpose()) * B;
	//std::cout << "B:\n\n" << B << "\n\nLAYL\n\n";

	// First let's solve L*x=`B`.
	vabor112::Matrix<double> tmp(B.get_width(), B.get_height());
	for (int k = 0; k < B.get_width(); k++) {
		for (int i = 0; i < B.get_height(); i++) {
			tmp(k, i) = B(k, i);
			for (int j = 0; j < i; j++) {
				tmp(k, i) -= tmp(k, j) * LU(j, i);
			}
		}
	}

	//std::cout << LU << "\n\n\n" << tmp << "\n\n\n";

	//std::cout << "YOBA\n";
	//Second we need to solve U*x=`tmp`.
	vabor112::Matrix<double> result(B.get_width(), B.get_height());
	for (int k = 0; k < tmp.get_width(); k++) {
		for (int i = tmp.get_height() - 1; i >= 0; i--) {
			result(k, i) = tmp(k, i);
			for (int j = tmp.get_width() - 1; j > i; j--) {
				result(k, i) -= result(k, j) * LU(j, i);
			}
			result(k, i) = result(k, i) / LU(i,i);
			//std::cout << k << " " << i << "\n\n" << result << "\n\n\n";
		}
	}

	//std::cout << "YOBA\n";

	return result;
}
