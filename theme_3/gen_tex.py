# -*- coding: utf-8 -*-

import sys
from numpy import poly1d, polyval, arange, exp
from math import factorial, sqrt, cos, pi, sin
import matplotlib.pyplot as plt
import csv

############################### General stuff #################################

def matrix_print(matrix):
    for row in matrix:
        for element in row:
            print element,
        print ''

def poly1d_to_tex(p, variable = 'x'):
    result = ''
    for i in xrange(p.o):
        if p.c[i] != 0:
            if p.o - i != 1:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s^%s'
                elif p.c[i] > 0:
                    template = '+ %s %s^%s'
                result += template % (p.c[i], variable, p.o - i)
            else:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s'
                elif p.c[i] > 0:
                    template = '+ %s %s'
                result += template % (p.c[i], variable)
    if (p.c[p.o] != 0) or (result == ''):
        if (p.c[p.o] <= 0) or (result == ''):
            template = '%s'
        elif p.c[p.o] > 0:
            template = '+ %s'
        result += template % p.c[p.o]
    return result

def sign(x):
    if x > 0:
        return 1
    elif x < 0:
        return -1
    elif x == 0:
        return 0

def diff_sign(diff_order):
    if diff_order <= 3:
        return '\'' * diff_order
    else:
        return '^{diff_order}'
    o

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

###############################################################################

# Compute divided difference for 'f' function of order 'n' and with nodes 'values'.
def divdiff(f, n, values):
    if n == 0:
        return f(values[0])
    else:
        return (divdiff(f, n-1, values[1:])-divdiff(f,n-1, values[:-1]))/(values[-1]-values[0])


# Compute theoretical error estimate for i'th interplation polynomial in
# target point 'target_point' with firs (i+1) nodes of 'nodes' and with
# (i+1)'th derivative estimate 'deriv_estimate'.
def theor_estimate(i, target_point, nodes, deriv_estimate):
    result = 1.0
    for x in xrange(i+1):
        result = result * (target_point - nodes[x])
    result = result / factorial(i+1)
    return abs(result * deriv_estimate)

# 'f' is a function we're going to approximate using equidistant
# nodes x_0, x_0 + h, x_0 + 2h, ..., x_0 + nh.
def newton_equidistant_interpolation(f, x_0, h, n, target_point,
        FINDIFFS_TABLE_NAME = 'findiffs_table.csv'):
    if n < 1:
        raise ValueError('Too few nodes')

    # Generating finite differences table.
    header = ['x', 'f(x)'] + [('к.р %s-го п.' % x) for x in xrange(1, n + 1)]
    values = [[None for x in xrange(len(header))] for x in xrange((n + 1)*2 - 1)]
    # We will start with some values
    for i in xrange(n+1):
        values[2*i][0] = x_0 + i*h
        values[2*i][1] = f(values[2*i][0])
    # Then recursively fill rest
    TABLE_HEIGHT = (n + 1)*2 - 1
    for cur_power in xrange(1, n + 1):
        for i in xrange(cur_power, TABLE_HEIGHT - cur_power + 1, 2):
            values[i][cur_power + 1] = values[i+1][cur_power - 1 + 1] - values[i-1][cur_power - 1 + 1]

    # The finite differences table is ready! Saving it and printing it.
    findiffs_table = [header] + values
    ####matrix_print(findiffs_table)

    with open(FINDIFFS_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows(findiffs_table)

    distances = [target_point - (x_0 + i*h) for i in xrange(n + 1)]
    current_node = closest_node = distances.index(min(distances, key = lambda x: abs(x)))
    sorted_nodes = sorted([x_0 + i*h for i in xrange(n + 1)], key = lambda x: abs(x - target_point))
    sorted_nodes = sorted(range(n + 1), key = lambda x: abs(x_0 + x*h - target_point))
    ## TODO: if closest_node == 0, then no actual interpolation is required.
    #interpolation_direction = 1 if distances[closest_node] >= 0 else -1

    cur_power = 0
    cur_row = 2 * closest_node
    cur_basis = poly1d([1])
    approximations = [poly1d(values[cur_row][1])]
    while cur_power < n:
        cur_power = cur_power + 1
        cur_row = cur_row + sign(sorted_nodes[cur_power] - closest_node)
        cur_approximation = approximations[-1]

        print 'Beginning approx: ', poly1d_to_tex(cur_approximation)

        #print cur_power, len(values), len(values[0])
        #print current_node, interpolation_direction

        #if ((0 > current_node + interpolation_direction)
        #        or (current_node + interpolation_direction >= (n + 1)*2 -1)
        #        or (values[current_node + interpolation_direction][1 + cur_power] == None)):
        #    interpolation_direction = -interpolation_direction
        cur_basis = cur_basis * poly1d([sorted_nodes[cur_power - 1] - closest_node], True)

        print 'Current basis: ', poly1d_to_tex(cur_basis)

        #print poly1d_to_tex(cur_basis)
        #print values[current_node + interpolation_direction][1 + cur_power]
        #print current_node, interpolation_direction
        #print poly1d_to_tex(values[current_node + interpolation_direction][1 + cur_power] * cur_basis / factorial(cur_power))

        ####print 'cur_power: ', cur_power, ', sorted_nodes[cur_power]: ', sorted_nodes[cur_power]
        ####print 'closest_node: ', closest_node
        ####print 'Cur row: ', cur_row
        print 'Fin diff: ', values[cur_row][1 + cur_power]

        cur_approximation += values[cur_row][1 + cur_power] * cur_basis / factorial(cur_power)

        #cur_approximation += values[current_node + interpolation_direction][1 + cur_power] * cur_basis / factorial(cur_power)
        approximations.append(cur_approximation)
        #current_node = current_node + interpolation_direction
        #interpolation_direction = -interpolation_direction

    print '\n\n'
    
    # Printing approximations.
    for approximation in approximations:
        print 'Got approximation: %s.' % poly1d_to_tex(approximation)
        print 'After x = %s, got: %s.' % (poly1d_to_tex(poly1d([1, -closest_node*h])), poly1d_to_tex(polyval(approximation, poly1d([1, -closest_node*h]))))
        print 'RESULT IS %s' % polyval(approximation, poly1d([1, -closest_node*h]))(target_point)
        #print 'RESULT IS %s' % approximation(target_point)

f = lambda x : x**5 + 2

newton_equidistant_interpolation(f, 0, 1, 5, 3.6)
