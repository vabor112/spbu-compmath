import os, re, shutil, tempfile, sympy, textwrap, numpy as np

from subprocess import Popen, PIPE

from sympy import latex

def TDMAsolver(a, b, c, d):
	'''
	TDMA solver, a b c d can be NumPy array type or Python list type.
	refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
	'''
	nf = len(a)	 # number of equations
	ac, bc, cc, dc = map(np.array, (a, b, c, d))	 # copy the array
	for it in xrange(1, nf):
		mc = ac[it]/bc[it-1]
		bc[it] = bc[it] - mc*cc[it-1] 
		dc[it] = dc[it] - mc*dc[it-1]

	xc = ac
	xc[-1] = dc[-1]/bc[-1]

	for il in xrange(nf-2, -1, -1):
		xc[il] = (dc[il]-cc[il]*xc[il+1])/bc[il]

	del bc, cc, dc  # delete variables from memory

	return xc


class PDFOutput():
	head = textwrap.dedent(
	u'''
		\\documentclass[a4paper, 12pt]{article}
		\\usepackage{mathtext}
		\\usepackage[T2A]{fontenc}
		\\usepackage[utf8x]{inputenc}
		\\usepackage[english, russian]{babel}
		\\usepackage[fleqn,tbtags]{amsmath}
		\\usepackage{mathtools}
		\\usepackage[left=1cm, right=1cm, top=1cm, bottom=2cm, bindingoffset=0cm]{geometry}
		\\usepackage{amsfonts}
		\\usepackage{amssymb,amsmath}
		\\usepackage{amsthm}

		\\newenvironment{absolutelynopagebreak}
		{\\par\\nobreak\\vfil\\penalty0\\vfilneg
		\\vtop\\bgroup}
		{\\par\\xdef\\tpd{\\the\\prevdepth}\\egroup
		\\prevdepth=\\tpd}

		\\begin{document}

	'''
	)

	bottom = textwrap.dedent(
	u'''	

		\\end{document}
	'''
	)

	def __init__(self):
		self.latex = u''

	def write(self, obj):
		if isinstance(obj, sympy.Basic):
			self.latex += '\n' + latex(obj, mode='equation', itex = True) + '\n'
		else:
			self.latex += unicode(obj)

	def generate_pdf(self, pdf_name):
		latex_file = tempfile.NamedTemporaryFile(mode = 'w', delete = False, suffix = '.tex')

		latex_file.write(self.head.encode('utf-8'))
		latex_file.write(self.latex.encode('utf-8'))
		latex_file.write(self.bottom.encode('utf-8'))
		latex_file.flush()
		self._compile_tex_file(latex_file, pdf_name)

	@staticmethod
	def _compile_tex_file(latex_file, outname):
		tex_name = latex_file.name
		pdf_name = os.path.splitext(latex_file.name)[0] + '.pdf'
		base_dir = os.path.dirname(tex_name)
		corrected_env = os.environ
		corrected_env['HOME'] = base_dir
		for i in xrange(2):
			pdflatex = Popen(["pdflatex", "-interaction=nonstopmode","-halt-on-error", "-file-line-error", latex_file.name], cwd = base_dir, env = corrected_env, stdout=PIPE, stderr=PIPE)
			(stdoutdata, stderrdata) = pdflatex.communicate()
			if not pdflatex.returncode == 0:
				errors = '\n'.join(re.findall("^.*:[0-9]*:.*$", stdoutdata, re.MULTILINE))
				os.system("cat %s" % tex_name)
				raise Exception(errors)

		shutil.copyfile(pdf_name, os.path.join(os.path.dirname(os.path.abspath(__file__)), outname))
		exts = ['.aux', '.tex', '.log', '.pdf']
		for ext in exts:
			os.unlink(os.path.splitext(latex_file.name)[0] + ext)

	@staticmethod
	def latex_table(m):
		width = len(m[0])
		height = len(m)

		first_row = [u'x\\textbackslash t'] + [(u'%.1f' % (0.2*i)) for i in xrange(6)]
		m = [first_row] + [[u'%.1f' % (0.2*i)] + [u'%.8f' % m[i][j] for j in xrange(0, width, width/5)] for i in xrange(0, height, height/5)]

		head = u'\\begin{tabular}{ l || %s }' % u' | '.join([u'l']*(width))
		latex_table = u''
		latex_table += u' & '.join(m[0]) + u'\\\\ \\hline \\hline \n'
		for row in m[1:]:
			latex_table += u' & '.join(row) + u'\\\\ \hline \n'
		bottom = u'\\end{tabular}'
		
		return (head + latex_table + bottom)


def print2d(m):
	for row in m:
		print u' '.join(unicode(cell) for cell in row)
		print ''

#class zero_f(Function):
#	nargs = 2
#
#	@classmethod
#	def eval(cls, x, y):
#		return 0
