# -*- coding: utf-8

from sympy import symbols, sympify, init_printing, diff, pprint, Function, Derivative, Eq, Subs, simplify, latex

from sympy.core.add import Add

from stuff import TDMAsolver, PDFOutput, print2d

init_printing(use_unicode=True)

output = PDFOutput()

x, t = symbols('x t')

uabs = Function('u')
pabs = Function('p')
babs = Function('b')
cabs = Function('c')
fabs = Function('f')

phiabs = Function('phi')
psiabs = Function('psi')
alphaabs = Function('alpha')
betaabs = Function('beta')

#aa = p
#bb = diff(p, x) + b
#cc = c

#exact_solution = sympify('3*x**2*t**2-t*x-2')
#exact_solution = sympify('3*x*t-2')

def generate_second_kind_hyperb_problem(p, b, c, exact_solution):

	f = diff(exact_solution, t, 2) - diff((x+1)*diff(exact_solution, x), x).doit()

	phi = exact_solution.subs(t, 0)
	psi = diff(exact_solution, t).subs(t, 0)

	alpha = diff(exact_solution, x).subs(x, 0)
	beta = exact_solution.subs(x, 1)

	return (f, phi, psi, alpha, beta)


def write_problem(p, b, c, f, phi, psi, alpha, beta, exact_solution, output):
	abs_eq = Eq(Derivative(uabs(x,t), t, t),
			Derivative(pabs(x,t)*Derivative(uabs(x,t), x), x)
			+ babs(x,t)*Derivative(uabs(x,t), x)
			+ cabs(x,t)*uabs(x,t)
			+ fabs(x,t))

	abs_cauchy1 = Eq(uabs(x,0), phiabs(x))
	abs_cauchy2 = Eq(Subs(uabs(x,t).diff(t), t, 0), psiabs(x))

	abs_border1 = Eq(Subs(uabs(x,t).diff(t), x, 0), alphaabs(t))
	abs_border2 = Eq(uabs(1, t), betaabs(t))

	a = (p*uabs(x,t).diff(x)).diff(x)
	b = b*uabs(x,t).diff(x)

	eq = Eq(uabs(x,t).diff(t,2),
			(p*uabs(x,t).diff(x)).diff(x)
			+ b*uabs(x,t).diff(x)
			+ c*uabs(x,t)
			+ f
			)

	cauchy1 = Eq(uabs(x,0), phi)
	cauchy2 = Eq(Subs(uabs(x,t).diff(t), t, 0), psi)

	border1 = Eq(Subs(uabs(x,t).diff(x), x, 0), alpha)
	border2 = Eq(uabs(1, t), beta)

	output.write(u'\n\nGenerating a problem of type:\n'.upper())

	output.write(abs_eq)
	output.write(abs_cauchy1)
	output.write(abs_cauchy2)
	output.write(abs_border1)
	output.write(abs_border2)

	output.write(u'\n\nFor the exact solution:\n'.upper())
	output.write(Eq(uabs(x,t), exact_solution))
	output.write(u'\n\nGot:\n'.upper())

	output.write(eq)
	output.write(cauchy1)
	output.write(cauchy2)
	output.write(border1)
	output.write(border2)

def explicit_scheme(N, M, p, b, c, f, phi, psi, alpha, beta, second_order_time_approximation = False):
	values = []
	for i in xrange(N+1):
		values.append([float('NaN')] * (M+1))
	
	#print2d(values)

	for i in xrange(N+1):
		values[i][0] = phi.subs(x, float(i)/N).evalf()

	for i in xrange(N+1):
		if second_order_time_approximation:
			Lphi = (diff(p*diff(phi, x), x) + b*diff(phi, x) + c*phi).subs(x, float(i)/N)
			values[i][1] = values[i][0] + psi.subs(x, float(i)/N)/float(M) + (Lphi + f.subs([(x, float(i)/N), (t, 0)]))/(2.0*N**2)
		else:
			values[i][1] = psi.subs(x, float(i)/N).evalf()/float(M)  + values[i][0]

	def Lh(i, k):
		result = p.subs(x, (float(i)+0.5)/N)*(values[i+1][k] - values[i][k])*N**2
		result += -p.subs(x, (float(i)-0.5)/N)*(values[i][k] - values[i-1][k])*N**2
		result += b.subs([(x, float(i)/N), (t, float(k)/M)])*(values[i+1][k] - values[i-1][k])*N/2.0
		result += c.subs([(x, float(i)/N), (t, float(k)/M)])*values[i][k]
		return result

	#def	Lhh(i, k):
	#	result = aa.subs([(x, float(i)/N), (t, float(k)/M)])*(values[i+1][k]-2*values[i][k]+values[i-1][k])*N**2
	#	result += bb.subs([(x, float(i)/N), (t, float(k)/M)])*(values[i+1][k]-values[i-1][k])*N/2.0
	#	result += c.subs([(x, float(i)/N), (t, float(k)/M)])*values[i][k]
	#	return result

	#def L(i, k):
	#	result = diff(p*diff(exact_solution, x), x) + b*diff(exact_solution, x) + c*exact_solution
	#	result = result.subs([(x, float(i)/N), (t, float(k)/M)])
	#	return result

	for k in xrange(1, M):
		for i in xrange(1,N):
			#values[i][k+1] = 2*values[i][k] - values[i][k-1] + (Lh(i,k) + f.subs([(x, i/N), (t, k/M)]))/float(M)**2
			values[i][k+1] = 2*values[i][k] - values[i][k-1] + (Lh(i,k) + f.subs([(x, float(i)/N), (t, float(k)/M)]))/float(M)**2
		
		values[0][k+1] = (alpha.subs(t, float(k+1)/M)*2/float(N) - 4*values[1][k+1]+values[2][k+1])/float(-3)
		values[N][k+1] = beta.subs(t, float(k+1)/M)

	return values

def explicit_scheme_second_order(N, M, p, b, c, f, phi, psi, alpha, beta):
	return explicit_scheme(N, M, p, b, c, f, phi, psi, alpha, beta, True)

def implicit_scheme(sigma, N, M, p, b, c, f, phi, psi, alpha, beta):
	values = []
	for i in xrange(N+1):
		values.append([float('NaN')] * (M+1))
	
	#print2d(values)

	for i in xrange(N+1):
		values[i][0] = phi.subs(x, float(i)/N).evalf()

	for i in xrange(N+1):
		values[i][1] = psi.subs(x, float(i)/N).evalf()/float(M)  + values[i][0]

	def Lh(i, k):
		result = p.subs(x, (float(i)+0.5)/N)*(values[i+1][k] - values[i][k])*N**2
		result += -p.subs(x, (float(i)-0.5)/N)*(values[i][k] - values[i-1][k])*N**2
		result += b.subs([(x, float(i)/N), (t, float(k)/M)])*(values[i+1][k] - values[i-1][k])*N/2.0
		result += c.subs([(x, float(i)/N), (t, float(k)/M)])*values[i][k]
		return result

	for k in xrange(1, M):
		A = [0.0] + [sigma*p.subs(x, (float(i)-0.5)/N)*N**2 for i in xrange(1, N)] + [0.0]

		B = [-float(N)]
		B += [-sigma*(N**2*(p.subs(x, (float(i)+0.5)/N) + p.subs(x, (float(i)-0.5)/N)) - c.subs([(x, float(i)/N), (t, float(k+1)/M)])) - M**2 for i in xrange(1, N)]
		B += [1.0]

		BB = [-a for a in B]

		C = [float(N)] + [sigma*(N**2*p.subs(x, (float(i)+0.5)/N) + b.subs([(x, float(i)/N), (t, float(k+1)/M)])*N/2.0) for i in xrange(1, N)] + [0.0]

		G = [alpha.subs(t, float(k+1)/M)]
		G += [M**2*(values[i][k-1]-2*values[i][k]) - (1.0-2.0*sigma)*Lh(i, k) - sigma*Lh(i, k-1) - f.subs([(x, float(i)/N), (t, float(k)/M)]) for i in xrange(1, N)]
		G += [beta.subs(t, float(k+1)/M)]

		xs = TDMAsolver(A, B, C, G)

		for i in xrange(N+1):
			values[i][k+1] = xs[i]

	return values

def implicit_scheme_zero(N, M, p, b, c, f, phi, psi, alpha, beta):
	return implicit_scheme(0.0, N, M, p, b, c, f, phi, psi, alpha, beta)
def implicit_scheme_half(N, M, p, b, c, f, phi, psi, alpha, beta):
	return implicit_scheme(0.5, N, M, p, b, c, f, phi, psi, alpha, beta)
def implicit_scheme_quarter(N, M, p, b, c, f, phi, psi, alpha, beta):
	return implicit_scheme(0.25, N, M, p, b, c, f, phi, psi, alpha, beta)

def exact_table(N, M, exact_solution):
	values = []
	for i in xrange(N+1):
		values.append([])
		for k in xrange(M+1):
			values[i].append(exact_solution.subs([(x, float(i)/N), (t, float(k)/N)]))
	
	return values

p = sympify('x+1')
b = sympify('0')
c = sympify('0')

tasks = [
		(sympify('3*x**2*t-t*x-2'), explicit_scheme, u'Explicit ($\\tau + h^2$)', [(5, 10), (10, 20), (20, 40)]),
		(sympify('x**4+t**3'), explicit_scheme, u'Explicit ($\\tau + h^2$)', [(5, 10), (10, 20), (20, 40)]),

		(sympify('3*x**2*t**2-t*x-2'), explicit_scheme_second_order, u'Explicit ($\\tau^2 + h^2$)', [(5, 10), (10, 20), (20, 40)]),
		(sympify('x**4+t**3'), explicit_scheme_second_order, u'Explicit ($\\tau^2 + h^2$)', [(5, 10), (10, 20), (20, 40)]),

		(sympify('3*x*t-t*x-2'), implicit_scheme_zero, u'Implicit ($\\sigma = 0$)', [(5, 10), (10, 20), (20, 40)]),
		(sympify('x**4+t**3'), implicit_scheme_zero, u'Implicit ($\\sigma = 0$)', [(5, 10), (10, 20), (20, 40)]),

		(sympify('3*x*t-t*x-2'), implicit_scheme_half, u'Implicit ($\\sigma = 0.5$)', [(5, 10), (10, 20), (20, 40)]),
		(sympify('x**4+t**3'), implicit_scheme_half, u'Implicit ($\\sigma = 0.5$)', [(5, 10), (10, 20), (20, 40)]),

		(sympify('3*x*t-t*x-2'), implicit_scheme_quarter, u'Implicit ($\\sigma = 0.25$)', [(5, 10), (10, 20), (20, 40)]),
		(sympify('x**4+t**3'), implicit_scheme_quarter, u'Implicit ($\\sigma = 0.25$)', [(5, 10), (10, 20), (20, 40)])
		]

for exact_solution, method, description, params_list in tasks:
	f, phi, psi, alpha, beta = generate_second_kind_hyperb_problem(p, b, c, exact_solution)

	output.write(u'{\\begin{center} \\LARGE %s \\end{center}' % description)
	write_problem(p, b, c, f, phi, psi, alpha, beta, exact_solution, output)

	output.write(u'\\begin{absolutelynopagebreak} \n Exact solution values: \\\\[1cm] \n')
	output.write(PDFOutput.latex_table(exact_table(5, 5, exact_solution)))
	output.write(u'\n \\end{absolutelynopagebreak} \n \\vspace{1cm} \n\n')

	for params in params_list:
		N = params[0] # for space
		M = params[1] # for time

		output.write(u'\\begin{absolutelynopagebreak} \n Approximation for $\\tau = %.3f, h = %.3f$ : \\\\[1cm] \n' % (1.0/M, 1.0/N))
		output.write(PDFOutput.latex_table(method(N, M, p, b, c, f, phi, psi, alpha, beta)))
		output.write(u'\n \\end{absolutelynopagebreak} \n \\vspace{1cm} \n')
	output.write(u'\n \\pagebreak \n')

output.generate_pdf('kudah.pdf')
