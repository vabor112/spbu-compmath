# -*- coding: utf-8 -*-

import sys
from numpy import poly1d, polyval, arange, exp
from math import factorial, sqrt, cos, pi, sin
import matplotlib.pyplot as plt
import csv

############################### General stuff #################################

def matrix_print(matrix):
    for row in matrix:
        for element in row:
            print element,
        print ''

def poly1d_to_tex(p, variable = 'x'):
    result = ''
    for i in xrange(p.o):
        if p.c[i] != 0:
            if p.o - i != 1:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s^%s'
                elif p.c[i] > 0:
                    template = '+ %s %s^%s'
                result += template % (p.c[i], variable, p.o - i)
            else:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s'
                elif p.c[i] > 0:
                    template = '+ %s %s'
                result += template % (p.c[i], variable)
    if (p.c[p.o] != 0) or (result == ''):
        if (p.c[p.o] <= 0) or (result == ''):
            template = '%s'
        elif p.c[p.o] > 0:
            template = '+ %s'
        result += template % p.c[p.o]
    return result

def sign(x):
    if x > 0:
        return 1
    elif x < 0:
        return -1
    elif x == 0:
        return 0

def diff_sign(diff_order):
    if diff_order <= 3:
        return '\'' * diff_order
    else:
        return '^{diff_order}'
    o

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

###############################################################################

# Compute divided difference for 'f' function of order 'n' and with nodes 'values'.
def divdiff(f, n, values):
    if n == 0:
        return f(values[0])
    else:
        return (divdiff(f, n-1, values[1:])-divdiff(f,n-1, values[:-1]))/(values[-1]-values[0])


# Compute theoretical error estimate for i'th interplation polynomial in
# target point 'target_point' with firs (i+1) nodes of 'nodes' and with
# (i+1)'th derivative estimate 'deriv_estimate'.
def theor_estimate(i, target_point, nodes, deriv_estimate):
    result = 1.0
    for x in xrange(i+1):
        result = result * (target_point - nodes[x])
    result = result / factorial(i+1)
    return abs(result * deriv_estimate)

# 'derivs_table' format example:
# [[0.0, -1.0, 3.0],
#  [3.0, 0.0],
#  [1.33, 12.3, -0.4, 8.8]]
# And we have
# derivs_table[i][1] = f(derivs_table[i][0])
# derivs_table[i][2] = f'(derivs_table[i][0])
# ..........................................
# derivs_table[i][j] = f^{(j)}(derivs_table[i][0])
#
# Remark about variable names: here we consider f(x) as f's 0 derivative.
def hermite_interpolation(derivs_table, INPUT_TABLE_NAME = 'input_table.csv',
        DIVDIFFS_TABLE_NAME = 'divdiffs_table_hermite.csv',
        APPROXIMATION_TEST_TABLE_NAME = 'approximation_test.csv'):

    max_given_derivatives = 0
    given_derivatives = 0
    for derivs_at_node in derivs_table:
        if len(derivs_at_node) < 2:
            raise ValueError('Too few values')
        given_derivatives = given_derivatives + len(derivs_at_node) - 1
        max_given_derivatives = max(max_given_derivatives, len(derivs_at_node) - 1)
        #given_derivatives = given_derivatives + len(filter(lambda x : x != None, derivs_at_node)) - 1
        #max_given_derivatives = max(max_given_derivatives,
        #        len(filter(lambda x : x != None, derivs_at_node)) - 1)

    #if max_given_derivatives < 1:
    #    raise ValueError('Too few values')
    print 'YOPTA', given_derivatives

    header = ['$x$'] + ['$f(x)%s$' % ('\''*x) for x in xrange(max_given_derivatives)] + [('р.р %s-го п.' % x) for x in xrange(1, given_derivatives)]
    values = [[None for x in xrange(len(header))] for x in xrange(given_derivatives*2 - 1)]
    print header
    print len(header)

    cur_node = 0
    for derivs_at_node in derivs_table:
        for i in xrange(len(derivs_at_node) - 1):
            for j in xrange(len(derivs_at_node)):
                print len(values), cur_node
                print 'len(values): %s, len(values[cur_node]: %s\ncur_node = %s, j = %s)' % (len(values), len(values[cur_node]), cur_node, j)
                values[cur_node][j] = derivs_at_node[j]
            cur_node = cur_node + 2
    

    #cur_node = 0
    #cur_deriv = 0
    #while cur_node < len(values):
    #    for i in xrange(len(derivs_table[cur_deriv]) - 1):
    #        for j in xrange(len(derivs_table[cur_deriv])):
    #            values[cur_node][j] = derivs_table[cur_deriv][j]
    #        cur_node = cur_node + 2
    #        cur_deriv = cur_deriv + 1
    #        print cur_deriv

    for cur_power in xrange(1, given_derivatives):
        for i in xrange(cur_power, len(values) - cur_power + 1, 2):
            divdiff_denominator = values[i-cur_power][0] - values[i+cur_power][0]
            if divdiff_denominator == 0:
                values[i][cur_power + max_given_derivatives] = values[i-cur_power][cur_power + 1] / factorial(cur_power)
            else:
                if cur_power == 1:
                    divdiff_numerator = values[i-1][1] - values[i+1][1]
                else:
                    divdiff_numerator = values[i-1][cur_power - 1 + max_given_derivatives] - values[i+1][cur_power - 1 + max_given_derivatives]
                values[i][cur_power + max_given_derivatives] = divdiff_numerator / divdiff_denominator

    divdiffs_table = [header] + values
    matrix_print(divdiffs_table)

    #print 'OYYOY', given_derivatives
    approximation = values[0][1]
    cur_base_polynomial = poly1d([1])
    cur_root_index = 0
    for cur_power in xrange(1, given_derivatives):
        #if values[cur_power][0] != None:
        #    root = values[cur_power][0]
        #else:
        #    root = values[cur_power - 1][0]
        #cur_base_polynomial = cur_base_polynomial * poly1d([root], True)
        cur_base_polynomial = cur_base_polynomial * poly1d([values[cur_root_index][0]], True)
        approximation = approximation + cur_base_polynomial * values[cur_power][max_given_derivatives + cur_power]
        cur_root_index += 2

    approximation_test_header = ['$x$'] + ['$P%s(x)$' % ('\''*i) for i in xrange(max_given_derivatives)]
    approximation_test_values = [[derivs_at_node[0]] + [(approximation.deriv(i))(derivs_at_node[0]) for i in xrange(len(approximation_test_header) - 1)] for derivs_at_node in derivs_table]
    matrix_print(approximation_test_values)
    approximation_test_table = [approximation_test_header] + approximation_test_values
    #approximation_test_table = [approximation_test_header] + [row + [None]*(max_given_derivatives + 1 - len(row)) for row in approximation_test_values]


    with open(INPUT_TABLE_NAME, 'w') as f:
        #print filter(lambda x: x[0] != None, divdiffs_table)
        #print set(filter(lambda x: x[0] != None, divdiffs_table))
        #print list(set(filter(lambda x: x[0] != None, divdiffs_table)))
        #csv.writer(f).writerows(list(set(filter(lambda x: x[0] != None, divdiffs_table))))
        #csv.writer(f).writerows(list(set(filter(lambda x: x[0] != None, divdiffs_table))))
        #csv.writer(f).writerows(derivs_table)
        derivs_table_header = ['$x$'] + ['$f(x)%s$' % ('\''*x) for x in xrange(max_given_derivatives)]
        csv.writer(f).writerows([derivs_table_header] + [row + [None]*(max_given_derivatives + 1 - len(row)) for row in derivs_table])
        

    with open(DIVDIFFS_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows(divdiffs_table)

    with open(APPROXIMATION_TEST_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows(approximation_test_table)


    from jinja2 import Environment, FileSystemLoader
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('hermite_template.tex')
    output_from_parsed_template = template.render(INPUT_TABLE_NAME = INPUT_TABLE_NAME,
            DIVDIFFS_TABLE_NAME = DIVDIFFS_TABLE_NAME,
            APPROXIMATION_TEST_TABLE_NAME = APPROXIMATION_TEST_TABLE_NAME,
            approximation = poly1d_to_tex(approximation))

    with open('hermite.tex', 'w') as f:
        f.write(unicode(output_from_parsed_template).encode('utf-8'))

    import subprocess, shlex
    proc=subprocess.Popen(shlex.split('xelatex hermite.tex'))
    proc.communicate()

    print 'Pdf file is successfully generated, look for hermite.pdf in current folder.'

#f = lambda x : x**2 + 2
#
#f_tex = 'x^2 + 2'
#
#nodes = [-2.0, 0.0, 1.0, 3.0, 4.0, 5.0]
#
#target_point = 2.0
#
#power = 3
#
##derivative_estimates = [12, 18, 6, 0]
#derivative_estimates = [12, 18, 6, 2, 0]

#the_table = [[-1.0, -17.0, 33.0],
#             [0.0, -4.0, 3.0, -8.0],
#             [2.0, 10.0]]

# 6th variant
the_table = [[0.0, 5.0, -7.0, 10.0],
             [1.0, -4.0, -13.0],
             [2.0, -61.0, -127.0],
             [3.0, -862.0]]

hermite_interpolation(the_table)
