# -*- coding: utf-8 -*-

import sys
from numpy import poly1d, polyval, arange, exp
from math import factorial, sqrt, cos, pi, sin
import matplotlib.pyplot as plt
import csv

############################### General stuff #################################

def matrix_print(matrix):
	for row in matrix:
		for element in row:
			print element,
		print ''

def poly1d_to_tex(p, variable = 'x'):
	result = ''
	for i in xrange(p.o):
		if p.c[i] != 0:
			if p.o - i != 1:
				if (result == '') or (p.c[i] < 0):
					template = '%s %s^%s'
				elif p.c[i] > 0:
					template = '+ %s %s^%s'
				result += template % (p.c[i], variable, p.o - i)
			else:
				if (result == '') or (p.c[i] < 0):
					template = '%s %s'
				elif p.c[i] > 0:
					template = '+ %s %s'
				result += template % (p.c[i], variable)
	if (p.c[p.o] != 0) or (result == ''):
		if (p.c[p.o] <= 0) or (result == ''):
			template = '%s'
		elif p.c[p.o] > 0:
			template = '+ %s'
		result += template % p.c[p.o]
	return result

def sign(x):
	if x > 0:
		return 1
	elif x < 0:
		return -1
	elif x == 0:
		return 0

def diff_sign(diff_order):
	if diff_order <= 3:
		return '\'' * diff_order
	else:
		return '^{diff_order}'
	o

def drange(start, stop, step):
	r = start
	while r < stop:
		yield r
		r += step

###############################################################################

# Integrate function 'f' on ['a', 'b'] with "N-noded" composite formulas.
# 'b' > 'a'.
# 't' is for type, t is a value of the list ['l', 'r', 'm'] which is for left,
# right and middle rectangles formulas.
def rectangles_comp(f, a, b, N, t = 'm'):
	a, b = float(a), float(b)
	h = (b-a)/N
	if t.lower() == 'm':
		alpha = a + h/2
	elif t.lower() == 'l':
		alpha = a
	elif t.lower() == 'r':
		alpha = a + h
	else:
		raise ValueError('wrong formula type')
		 
	nodes = [(alpha + k*h) for k in xrange(N)]

	return h*sum(map(f, nodes))

def trapezoidal_comp(f, a, b, N):
	a, b = float(a), float(b)
	h = (b-a)/N
	nodes = [(a + k*h) for k in xrange(1, N)]
	return h*((f(a) + f(b))/2 + sum(map(f, nodes)))

def simpson(f, a, b):
	return (f(a) + 4*f((a + b)/2) + f(b))*(b - a)/6

def simpson_comp(f, a, b, N):
	a, b = float(a), float(b)
	h = (b-a)/N
	nodes = [(a + k*h) for k in xrange(0, N)]
	return sum([simpson(f, node, node + h) for node in nodes])

def integration(f, a, b, N, integral, deriv_1_estimate, deriv_2_estimate, deriv_4_estimate,
		INTEGRATION_TABLE_NAME = 'integration.csv'):
	table_header = ['Метод', 'S_N', 'I-S_N', 'R_N', 'S_2N', 'I-S_2N', 'R_2N', 'R_main', 'I_ad', 'I-I_ad']
	def R_N_DIV_C(f, a, b, N, d, deriv_estimate):
		return float(b-a) * (((b-a)/float(N))**(d + 1)) * deriv_estimate
	row1 = ['Левых прямоугольников']
	row1 += [rectangles_comp(f,a,b,N,'l'), integral - rectangles_comp(f,a,b,N,'l'), (1/2.0)*R_N_DIV_C(f,a,b,N,0,deriv_1_estimate)]
	row1 += [rectangles_comp(f,a,b,2*N,'l'), integral - rectangles_comp(f,a,b,2*N,'l'), (1/2.0)*R_N_DIV_C(f,a,b,2*N,0,deriv_1_estimate)]
	row1 += [(row1[4]-row1[1])/(2**(0+1)-1)]
	row1 += [row1[4] + row1[-1]]
	row1 += [integral - row1[-1]]

	row2 = ['Правых прямоугольников']
	row2 += [rectangles_comp(f,a,b,N,'r'), integral - rectangles_comp(f,a,b,N,'r'), (1/2.0)*R_N_DIV_C(f,a,b,N,0,deriv_1_estimate)]
	row2 += [rectangles_comp(f,a,b,2*N,'r'), integral - rectangles_comp(f,a,b,2*N,'r'), (1/2.0)*R_N_DIV_C(f,a,b,2*N,0,deriv_1_estimate)]
	row2 += [(row2[4]-row2[1])/(2**(0+1)-1)]
	row2 += [row2[4] + row2[-1]]
	row2 += [integral - row2[-1]]

	row3 = ['Средних прямоугольников']
	row3 += [rectangles_comp(f,a,b,N,'m'), integral - rectangles_comp(f,a,b,N,'m'), (1/24.0)*R_N_DIV_C(f,a,b,N,1,deriv_2_estimate)]
	row3 += [rectangles_comp(f,a,b,2*N,'m'), integral - rectangles_comp(f,a,b,2*N,'m'), (1/24.0)*R_N_DIV_C(f,a,b,2*N,1,deriv_2_estimate)]
	row3 += [(row3[4]-row3[1])/(2**(1+1)-1)]
	row3 += [row3[4] + row3[-1]]
	row3 += [integral - row3[-1]]

	row4 = ['Трапеций']
	row4 += [trapezoidal_comp(f,a,b,N), integral - trapezoidal_comp(f,a,b,N), (1/12.0)*R_N_DIV_C(f,a,b,N,1,deriv_2_estimate)]
	row4 += [trapezoidal_comp(f,a,b,2*N), integral - trapezoidal_comp(f,a,b,2*N), (1/12.0)*R_N_DIV_C(f,a,b,2*N,1,deriv_2_estimate)]
	row4 += [(row4[4]-row4[1])/(2**(1+1)-1)]
	row4 += [row4[4] + row4[-1]]
	row4 += [integral - row4[-1]]

	row5 = ['Симпсона']
	row5 += [simpson_comp(f,a,b,N), integral - simpson_comp(f,a,b,N), (1/2880.0)*R_N_DIV_C(f,a,b,N,3,deriv_4_estimate)]
	print integral, simpson_comp(f,a,b,N), integral - simpson_comp(f,a,b,N)
	row5 += [simpson_comp(f,a,b,2*N), integral - simpson_comp(f,a,b,2*N), (1/2880.0)*R_N_DIV_C(f,a,b,2*N,3,deriv_4_estimate)]
	row5 += [(row5[4]-row5[1])/(2**(3+1)-1)]
	row5 += [row5[4] + row5[-1]]
	row5 += [integral - row5[-1]]

	table = [table_header, row1, row2, row3, row4, row5]
	table = [[row[0]]+['']+row[1:4]+['']+row[4:7]+['']+row[7:10] for row in table]

	with open(INTEGRATION_TABLE_NAME, 'w') as f:
		csv.writer(f).writerows(table)


f = lambda x : 1.0 / (x**2+4.0)

p_0 = lambda x : 5.0 # integral = 5.0
p_1 = lambda x: 3.0-x # integral = 2.5
p_2 = lambda x: x**2-3.0*x+4.0 # integral = 2.83333333333333333
p_3 = lambda x: x**3-2.0*x**2+5.0*x-3.0 # integral = -0.9166666666666667

#integration(f, a, b, N, integral, deriv_1_estimate, deriv_2_estimate, deriv_4_estimate, INTEGRATION_TABLE_NAME = 'integration.csv')
integration(p_0, 0.0, 1.0, 2, 5.0, 0.0, 0.0, 0.0, INTEGRATION_TABLE_NAME = 'poly_0_integration.csv')
integration(p_1, 0.0, 1.0, 2, 2.5, 1.0, 0.0, 0.0, INTEGRATION_TABLE_NAME = 'poly_1_integration.csv')
integration(p_2, 0.0, 1.0, 2, 2.83333333333333333, 3.0, 2.0, 0.0, INTEGRATION_TABLE_NAME = 'poly_2_integration.csv')
integration(p_3, 0.0, 1.0, 2, -0.9166666666666667, 5.0, 4.0, 0.0, INTEGRATION_TABLE_NAME = 'poly_3_integration.csv')

integration(f, 0.0, 1.0, 2, 0.23182380450040305, 1.0/8.0, 1.0/8.0, 24.0/64.0)
