# -*- coding: utf-8 -*-

import sys
from numpy import poly1d, polyval, arange, exp
from math import factorial, sqrt, cos, pi, sin
import matplotlib.pyplot as plt
import csv

############################### General stuff #################################

def matrix_print(matrix):
    for row in matrix:
        for element in row:
            print element,
        print ''

def poly1d_to_tex(p, variable = 'x'):
    result = ''
    for i in xrange(p.o):
        if p.c[i] != 0:
            if p.o - i != 1:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s^%s'
                elif p.c[i] > 0:
                    template = '+ %s %s^%s'
                result += template % (p.c[i], variable, p.o - i)
            else:
                if (result == '') or (p.c[i] < 0):
                    template = '%s %s'
                elif p.c[i] > 0:
                    template = '+ %s %s'
                result += template % (p.c[i], variable)
    if (p.c[p.o] != 0) or (result == ''):
        if (p.c[p.o] <= 0) or (result == ''):
            template = '%s'
        elif p.c[p.o] > 0:
            template = '+ %s'
        result += template % p.c[p.o]
    return result

def sign(x):
    if x > 0:
        return 1
    elif x < 0:
        return -1
    elif x == 0:
        return 0

def diff_sign(diff_order):
    if diff_order <= 3:
        return '\'' * diff_order
    else:
        return '^{diff_order}'
    o

def drange(start, stop, step):
    r = start
    while r < stop:
        yield r
        r += step

###############################################################################

# Compute divided difference for 'f' function of order 'n' and with nodes 'values'.
def divdiff(f, n, values):
    if n == 0:
        return f(values[0])
    else:
        return (divdiff(f, n-1, values[1:])-divdiff(f,n-1, values[:-1]))/(values[-1]-values[0])


# Compute theoretical error estimate for i'th interplation polynomial in
# target point 'target_point' with firs (i+1) nodes of 'nodes' and with
# (i+1)'th derivative estimate 'deriv_estimate'.
def theor_estimate(i, target_point, nodes, deriv_estimate):
    result = 1.0
    for x in xrange(i+1):
        result = result * (target_point - nodes[x])
    result = result / factorial(i+1)
    return abs(result * deriv_estimate)

def newton_interpolation(f, nodes, target_point, power, derivative_estimates,
        f_tex = 'f(x)', DIVDIFFS_TABLE_NAME = 'divdiffs.csv', RESULTS_TABLE_NAME = 'results.csv'):
    if len(nodes) < power + 1:
        raise ValueError('Too few nodes')

    nodes = sorted(nodes)

    # Generating divided differences table.
    header = ['x', 'f(x)'] + [('р.р %s-го п.' % x) for x in xrange(1, power+1)]
    values = [[None for x in xrange(len(header))] for x in xrange(len(nodes)*2 - 1)]
    # We will start with some values
    for i in xrange(len(nodes)):
        values[2*i][0] = nodes[i]
        values[2*i][1] = f(nodes[i])
    # Then recursively fill rest
    TABLE_WIDTH = 2 + power
    TABLE_HEIGHT = len(nodes)*2 - 1
    for cur_power in xrange(1, power + 1):
        for i in xrange(cur_power, TABLE_HEIGHT - cur_power + 1, 2):
            divdiff_numerator = values[i-1][cur_power - 1 + 1] - values[i+1][cur_power - 1 + 1]
            divdiff_denominator = values[i-cur_power][0] - values[i+cur_power][0]
            values[i][cur_power + 1] =  divdiff_numerator / divdiff_denominator
    # The divided differences table is ready! Saving it and printing it.
    divdiffs_table = [header] + values
    matrix_print(divdiffs_table)

    # Now we will gain polynomial approximations themself.
    # Here we forget about computed divided differences(we're such a sinners)
    # and use the divdiff() function. Just because it's easier not to
    # mess with the table.
    nodes.sort(key = lambda x: abs(x - target_point)) # Sorting nodes in order of proximity to target_point.

    # Setting initial approximation.
    approximations = [poly1d(f(nodes[0]))]
    # Now recursively comute the rest.
    for i in xrange(1, power + 1):
        cur_approximation = poly1d([nodes[0]], True)
        for x in xrange(1,i):
            cur_approximation = cur_approximation * poly1d([nodes[x]], True)
        cur_approximation = cur_approximation * divdiff(f, i, nodes[:(i+1)])
        approximations.append(approximations[-1] + cur_approximation)
    
    # Printing approximations.
    for approximation in approximations:
        print approximation
        print 'RESULT IS %s' % approximation(target_point)

    # Generation of results table.
    header = ['i'] + ['%s' % i for i in xrange(power+1)]
    row1 = ['Узлы в порядкe очередности их использования'] + nodes[:(power+1)] # $$ is a dirty hack for latex's csvsimple
    row2 = ['$P_i(%s)$ — значение многочлена в точке интерполирования' % target_point] + [x(target_point) for x in approximations]
    row3 = ['$f(%s) - P_i(%s)$' % (target_point, target_point)] + [f(target_point)-x(target_point) for x in approximations]
    row4 = ['$M_{i+1}$ — оценка модуля произв.'] + derivative_estimates
    row5 = ['$R_i(2)$ — оценка погрешности'] + [theor_estimate(i, target_point, nodes, derivative_estimates[i]) for i in xrange(power+1)]

    results_table = [header, row1, row2, row3, row4, row5]


    # Printing results table.
    matrix_print(results_table)

    # Now generating some beautiful tex.
    with open(DIVDIFFS_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows(divdiffs_table)

    with open(RESULTS_TABLE_NAME, 'w') as f:
        csv.writer(f).writerows(results_table)

    from jinja2 import Environment, FileSystemLoader
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('newton_template.tex')
    output_from_parsed_template = template.render(f_tex = f_tex, nodes = nodes, power = power,
            DIVDIFFS_TABLE_NAME = DIVDIFFS_TABLE_NAME, RESULTS_TABLE_NAME = RESULTS_TABLE_NAME,
            approximations = map(poly1d_to_tex, approximations))
    with open('newton.tex', 'w') as f:
        f.write(unicode(output_from_parsed_template).encode('utf-8'))

    import subprocess, shlex
    proc=subprocess.Popen(shlex.split('xelatex newton.tex'))
    proc.communicate()

    print 'Pdf file is successfully generated, look for newton.pdf in current folder.'

def lagrange_interpolation(nodes, values, power):
    approximations = []
    for p in xrange(power + 1):
        approximation = poly1d([0])
        for i in xrange(p + 1):
            cur_approximation = poly1d([1])
            for j in xrange(p + 1):
                if i != j:
                    cur_approximation = cur_approximation * poly1d([nodes[j]], True)
                    cur_approximation = cur_approximation / (nodes[i] - nodes[j])
            #print cur_approximation
            cur_approximation = cur_approximation * values[i]
            #print cur_approximation
            approximation = approximation + cur_approximation
        approximations.append(approximation)
    return approximations


def lagrange_back_interpolation(f, nodes, target_point, power, f_tex):
    nodes.sort(key = lambda x: abs(x - target_point)) # Sorting nodes in order of proximity to target_point.
    values = map(f, nodes)
    nodes, values = values, nodes # We're solving back interpolation task, don't we.
    print nodes
    print values

    if len(nodes) < power + 1:
        raise ValueError('Too few nodes')

    approximations = lagrange_interpolation(nodes, values, power)

    results = []
    for approximation in approximations:
        print approximation
        print 'RESULT IS %s' % approximation(target_point)
        results.append(approximation(target_point))

    from jinja2 import Environment, FileSystemLoader
    env = Environment(loader=FileSystemLoader('.'))
    template = env.get_template('lagrange_template.tex')
    output_from_parsed_template = template.render(f_tex = f_tex, nodes = values, values = nodes, power = power,
            approximations = map(poly1d_to_tex, approximations), results = results, target_point = target_point)
    with open('lagrange.tex', 'w') as f:
        f.write(unicode(output_from_parsed_template).encode('utf-8'))

    import subprocess, shlex
    proc=subprocess.Popen(shlex.split('xelatex lagrange.tex'))
    proc.communicate()

    print 'Pdf file is successfully generated, look for lagrange.pdf in current folder.'

# 'n' is the number of nodes.
def lagrange_interpolation_special_nodes(f, n):
    #for k in xrange(1, n + 1):
    #    print k
    #    print pi/2
    #    print ((2*k - 1)/n)
    #    print (pi/2) * ((2*k - 1)/n)
    nodes = sorted([cos((pi/2) * ((2*k - 1)/float(n))) for k in xrange(1, n + 1)])
    values = map(f, nodes)
    approximation_cheb = lagrange_interpolation(nodes, values, n - 1)[-1]

    print 'Chebyshev nodes: %s.\n Values at Chebyshev nodes: %s' % (nodes, values)
    print 'Got approximation: %s' % poly1d_to_tex(approximation_cheb)

    n = n - 1 # To exclude edge nodes overhead(just look at nest line).
    nodes = [-1 + (2*i)/float(n) for i in xrange(n + 1)]
    values = map(f, nodes)
    approximation_equidistant = lagrange_interpolation(nodes, values, n)[-1]

    print 'Equidistant nodes: %s.\n Values at equidistant nodes: %s' % (nodes, values)
    print 'Got approximation: %s' % poly1d_to_tex(approximation_equidistant)

    x = arange(-5.0, 5.0, 0.001)

    fig = plt.figure()
    fig.suptitle('Comparing f(in red) and f\'s approximations\nwith Chebyshev\'s nodes(in green), with equidistant nodes(in blue)\n', fontsize=11, fontweight='bold')
    plt.plot(x, map(f, x), 'r', x, map(approximation_cheb, x), 'g', x, map(approximation_equidistant, x), 'b')
    plt.axis([-5, 5, -5, 5])
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

f = lambda x : x**3+2

f_tex = 'x^3 + 2'

nodes = [-2.0, 0.0, 1.0, 3.0, 4.0, 5.0]

target_point = 2.0

power = 3

#derivative_estimates = [10, 2, 0, 0]
derivative_estimates = [75, 30, 6, 0]

#newton_interpolation(f, nodes, target_point, power, derivative_estimates, f_tex)
#
#lagrange_back_interpolation(f, nodes, target_point, power, f_tex)

lagrange_interpolation_special_nodes(lambda x: 1/(1+25*x**2), 35)
